let room_switch_list = document.querySelectorAll(".switcher_block")

if (room_switch_list.length != 0) {
    let li_head = room_switch_list[0].querySelectorAll(".switcher_button");
    let li_description = room_switch_list[1].querySelectorAll(".switcher_block_wrap");
    for (const li of li_head) {
        li.addEventListener("click", change_description)
    }
    function change_description() { 
        for (const li of li_head) {
            li.classList.remove("active")
        }
        for (const li of li_description) {
            li.classList.remove("active")
        }
        let actual_number = this.dataset.num; 
        li_description[actual_number].classList.add("active")
        this.classList.add('active')
    }
}