const mainVideo = document.getElementsByClassName('main_video_block')

if (mainVideo.length > 0) {
    let videoPlayer = document.querySelector('.main_video_img_wrap')
    videoPlayer.addEventListener('touchstart', videoEventButton)
    videoPlayer.addEventListener('click', videoEventButton)
}

function videoEventButton(event) {
    event.preventDefault();
    let videoBlock = document.querySelector('.main_video_img')
    let videoYoutube = document.querySelector('.main_video')
    videoBlock.classList.add('active')
    videoYoutube.classList.add('active')
}


const iconPlayerBlock = document.querySelectorAll(".hotel_img_wrap");
iconPlayerBlock.forEach((item) => {
    item.addEventListener('touchstart', videoEventHandlers)
    item.addEventListener('click', videoEventHandlers)
})

function videoEventHandlers(event) {
    event.preventDefault();
    let videoBlock = this.closest('.hotel_img')
    let parentVideoPlayer = this.closest('.about-hotel-video_wrap')
    let videoPlayer = parentVideoPlayer.querySelector('.iframe_video_play')
    this.classList.add('active')
    videoBlock.classList.add('active')
    videoPlayer.classList.add('active')
}





