let switcher_block = document.querySelector(".switcher_block");

if (switcher_block) {
    let switcher_line = switcher_block.querySelector(".room_switch_block_line");
    let switcher_button = switcher_block.querySelector(".switcher_button.active");
    switcher_line.style.cssText = "width: " + switcher_button.offsetWidth + "px;";

    let switcher_buttons = switcher_block.querySelectorAll(".switcher_button"); 
    for (const button of switcher_buttons) {
        button.addEventListener("click", clickSwitcher)
    }


function clickSwitcher() {
    let screen_size = window.screen;
    let screen_width = screen_size.width;
    let screen_height = screen_size.height;
//    console.log(screen_width);
//    console.log(screen_height);

    let railWidth = this.offsetWidth; 
    let number = this.dataset.num;
    
    let position = 0; // how px

 
        for (let i=0; i < number; i++) {
            let marginRight = getComputedStyle( switcher_buttons[i] ).marginRight.replace("px", "");
            position += switcher_buttons[i].offsetWidth + Number(marginRight);
        }
    
    switcher_line.style.cssText = "width: " + railWidth + "px; left: " + position + "px";
}

}
