const starButton = document.querySelectorAll(".modal-star-btn");
starButton.forEach((item) => {
    item.addEventListener('touchstart', starEventHandlers)
    item.addEventListener('click', starEventHandlers)
})

function starEventHandlers() {
    let dataModalValue = this.getAttribute('data-value'); 
    let starBlock = document.querySelector('.modal-mark-star');
    let input_star = starBlock.querySelector(".almira_rating");
    input_star.value = dataModalValue;
    starBlock.dataset.value = dataModalValue;
    clearStartActive();
    for(let i = 0; i < dataModalValue; i++) {
        starButton[i].classList.add('star_active')
    }
}

function clearStartActive() {
    for(let i = 0; i < starButton.length; i++) {
        starButton[i].classList.remove('star_active')
    }
}