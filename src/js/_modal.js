const openModalButtonLenght = document.getElementsByClassName('feed_info_block_btn ').length;
if(openModalButtonLenght > 0) {

    let openModalButtons = document.querySelectorAll('[data-modal-target]')
    let closeModalButtons = document.querySelectorAll('[data-close-button]')
    console.log(closeModal);
    let overlay = document.getElementById('overlay')
    const closeModalByButton = document.getElementById('close-modal-button')
    const closeModalSuccess = document.getElementById('success-close-button')
    closeModalSuccess.addEventListener('click', closeModalSuccessEvent)
    closeModalByButton.addEventListener('click', closeModalByButtonEvent)

    function closeModalSuccessEvent() {
        closeModal(modal)
    }

    function closeModalByButtonEvent() {
        closeModal(modal)
    }

    openModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = document.querySelector(button.dataset.modalTarget)
            openModal(modal)
            openActiveModal()
        })
    })

    function openActiveModal() {
        const modals = document.querySelectorAll('.modal-entry.active')
        if (modals.length == 1) {
            overlay.addEventListener('click', () => {
                modals.forEach(modal => {
                    closeModal(modal)
                })
            })
        }
    }


    closeModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = button.closest('.modal-entry')
            closeModal(modal)
        })
    })

    function openModal(modal) {
        if (modal == null) return
        modal.classList.add('active')
        overlay.classList.add('active')
    }

    function closeModal(modal) {
        if (modal == null) return
        modal.classList.remove('active')
        overlay.classList.remove('active')
    }

    const textarea = document.querySelector('.modal_textarea');
    const sendFormBtn = document.querySelector('.modal-send-button');
    const holderText = document.querySelector('.modal_textarea');
    sendFormBtn.addEventListener('click', sendEvent);

    function sendEvent() {
        if(textarea.value === '') {
            holderText.placeholder = "Неверно указаны значения";
            holderText.classList.add('active')
        }
    }

}
