/*
  "use strict";
//  import $ from 'jquery';
// import jQuery from 'jquery';
  import '../../node_modules/jquery/dist/jquery';

  window.$ = jQuery;

  if ($) {
      // require('bootstrap');

      //JS files
      require ("./base");
      require ("./catalog");
      //require ("./libs/fancybox/jquery.fancybox");
      //style
      require ("../scss/main.scss");
      require ("@fancyapps/fancybox");
}
*/
// Almira syles
import "./libs/jquery.fancybox";
import "./libs/owl.carousel";
import "./libs/jquery-ui";
import "./libs/jquery.ui.touch-punch";
import "./_map";

//JS files
import "./base";
import "./catalog";
import "./video";
import "./room_switcher"; 
import "./_rails-effect";
import "./_room-item";
import "./_modal";
import "./modal_star";
import "./line_back_switcher";
import "./line_hover_switcher"
  

// Stylesheet
import "../scss/main.scss";