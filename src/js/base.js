//const { event } = require("jquery");

//aside toggler.js 
let aside = document.querySelector(".navigation");
let aside_buttons = document.querySelector(".navigation__button-toggle");
let aside_selecting = false;

let aside_phone = aside.querySelector(".navigation__phone");



aside.addEventListener("transitionend", (event)=> {  
    if (aside.classList.contains("active")) {
        aside_selecting = true; 
    } else {
        aside_selecting = false;
    } 
    //addEvent();
});

addEvent();
 
function addEvent() {  
    aside.addEventListener("touchmove", function(event) {
        event.preventDefault();
    })
    aside.addEventListener("mouseover", (event) => {aside_toggle(event)});
    aside.addEventListener("mouseout", (event) => {aside_toggle(event)});
    aside_buttons.addEventListener("click", (event) => {aside_toggle(event)});
}

function removeEvent() {  
    aside.removeEventListener("touchmove", function(event) {
        event.preventDefault();
    })
    aside.removeEventListener("mouseover", (event) => {aside_toggle(event)});
    aside.removeEventListener("mouseout", (event) => {aside_toggle(event)});
    aside_buttons.removeEventListener("click", (event) => {aside_toggle(event)});
}

function aside_toggle(event) {  

    if (event.target != aside_phone) {

        if (event.type === "click") {
            if (aside_selecting) {
                aside.classList.remove("active"); 
            } else {
                aside.classList.add("active"); 
            }
        }

        if (event.type === "mouseout" ) {
            aside.classList.remove("active"); 
        }

        if (event.type === "mouseover" ) {
            aside.classList.add("active"); 
        }
    }
    removeEvent();
} 

