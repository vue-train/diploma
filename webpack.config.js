const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const FileManagerPlugin = require('filemanager-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  mode: "development",
  context: path.resolve(__dirname, "src"),
  entry: {
    main: "./js/index.js",
    // img: './img/',
  },
  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
  },

  resolve: {
    extensions: [".js", ".json", ".png", "svg", "gif", "jpeg", "jpg", "cur", "pptx"],
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@models": path.resolve(__dirname, "src/models"),
    },
  },
  devServer: {
    port: 4200,
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: "./index.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "product.html",
      template: "./product.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "commercial.html",
      template: "./commercial.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "manager-form.html",
      template: "./manager-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "designer-form.html",
      template: "./designer-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "designer-main-form.html",
      template: "./designer-main-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "manager-form.html",
      template: "./manager-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "cutter-form.html",
      template: "./cutter-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "seamstress-form.html",
      template: "./seamstress-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "sealer-form.html",
      template: "./sealer-form.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "employ.html",
      template: "./employ.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "promotion.html",
      template: "./promotion.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "profile.html",
      template: "./profile.html",
      minify: false,
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new HTMLWebpackPlugin({
      filename: "structure.html",
      template: "./structure.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "structure-item.html",
      template: "./structure-item.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "addUser.html",
      template: "./addUser.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "rolesEntry.html",
      template: "./rolesEntry.html",
      minify: false,
    }),
    new HTMLWebpackPlugin({
      filename: "roles.html",
      template: "./roles.html",
      minify: false,
    }),

    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].bundle.css",
    }),
    new CopyWebpackPlugin({
      patterns: [
        { from: "files", to: "files" },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: "babel-loader",
            options: {
              presets: ['@babel/preset-env']
            }
        }
    },

      {
        test: /\.html$/,
        include: path.resolve(__dirname, "src/template"),
        loader: "html-loader",
        options: {
          // Disables attributes processing
          minimize: false,
        }
      },

      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader","postcss-loader"],
      },
      {
        test: /\.less$/,
        exclude: /node_modules/,
        use: [MiniCssExtractPlugin.loader, "css-loader",  'postcss-loader' ,  "less-loader"],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true, },},
          'postcss-loader' ,
          { loader: "sass-loader", options: { sourceMap: true, },},
      ],
      },
      {
        test: /\.(png|svg|gif|jpg|jpeg|cur|pptx)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot|otf)$/,
        loader: "file-loader",
        options: {
          name: "[path][name].[ext]",
        },
      },
    ],
  },
};
